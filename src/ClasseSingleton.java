import javax.jws.Oneway;
// finale pour empcher d'heriter la classe Singleton
public final   class ClasseSingleton {
    @Override
    public String toString() {
        return "ClasseSingleton{" +
                "param1='" + param1 + '\'' +
                ", parma2='" + parma2 + '\'' +
                ", entier=" + entier +
                '}';
    }
 // synchronized est utilisé dans le contexte multi thread
    public synchronized String getParam1() {
        return param1;
    }

    public synchronized void setParam1(String param1) {
        this.param1 = param1;
    }

    public synchronized String getParma2() {
        return parma2;
    }

    public  synchronized void setParma2(String parma2) {
        this.parma2 = parma2;
    }

    public synchronized  int getEntier() {
        return entier;
    }

    public  synchronized void setEntier(int entier) {
        this.entier = entier;
    }

    String param1 = "param1";
    String parma2 = "param2";
    int entier =12;
    private  final  static ClasseSingleton  classeSingleton = new ClasseSingleton();
//constructeur
    private ClasseSingleton() {}

    public static  ClasseSingleton getClasseSingleton() {
        return classeSingleton;
    }
}
